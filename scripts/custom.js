$(document).ready(function(){
    //Mobile navigation
    $( "#mob-menu" ).click(function() {
        $( ".navigation" ).stop().slideToggle();
    });

    //Hearts active
    $('.fa-heart').click(function(){
        $(this).toggleClass('active');
    });

    //Colours active
    $('.one').click(function(){
        $(this).toggleClass('active-colour');
        $('.two').removeClass('active-colour');
    });

    $('.two').click(function(){
        $(this).toggleClass('active-colour');
        $('.one').removeClass('active-colour');
    });

    //Bootstrap Carousel for products
    $('#carouselExample').on('slide.bs.carousel', function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $('.carousel-item').length;
        
        if (idx >= totalItems-(itemsPerSlide-1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i=0; i<it; i++) {
                // append slides to end
                if (e.direction=="left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                }
                else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });
});