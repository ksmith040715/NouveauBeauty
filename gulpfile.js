/*!
    gulp
    npm install gulp-sass gulp-autoprefixer gulp-minify-css gulp-concat gulp-watch gulp-uglify gulp-notify gulp-rename --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    watch = require('gulp-watch'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload');

// Styles
gulp.task('styles', function() {
    return gulp.src([
            'styles/scss/main.scss'
        ])
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(concat('main.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('styles/css'));
});

// Watch
gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch('styles/**/*.scss', ['styles']);
});